﻿using System;
using System.Collections.Generic;

namespace ex_23
{
    class Program
    {
        static void Main(string[] args)
        {
            //---------------------------------
            // Exercise #23 - Dictionaries
            //---------------------------------
            // Note: You only need a single repository for this task - everything can be coded in a single Program.cs
            // Note 2: When you go on to the next question, watch out for the names of the variables.
            // Note 3: If it helps with your program flow, write out a flow diagram to help you with your program

            // a) Create a dictionary with 5 items and for every movie add the genre to it.
            // (key = title, value = genre)
            Console.WriteLine("------------------- A -------------------");
            var dicMovies = new Dictionary<string, string>();

            dicMovies.Add("Lord of the ring", "Fantasy");
            dicMovies.Add("Love Actually", "Drama");
            dicMovies.Add("Matrix", "Fantasy");
            dicMovies.Add("Kit Kit Kit", "Comedies");
            dicMovies.Add("Hello World", "Action");

            foreach(var movie in dicMovies)
            {
                Console.WriteLine($"The movie is '{movie.Key}', genre is '{movie.Value}'");
            }  

            
            // b) Print the names of the movies in a genre of your choosing. For this you will need to run a foreach loop. 
            // In the body of a loop you can run other code, you can change the value of a variable or add items to a list
            Console.WriteLine("\n\n------------------ B --------------------");

            foreach(var movie in dicMovies)
            {
                Console.WriteLine($"\nThe movie is '{movie.Key}', genre is '{movie.Value}'");

                Console.Write("Would you like to change the genre? (yes or no) : ");
                if(Console.ReadLine() == "yes")
                {
                    var movieGenre = "";

                    Console.Write("Type in the new movie genre : ");
                    movieGenre = Console.ReadLine();

                    Console.Write("You've changed the movie title and genre");
                    Console.Write($"Before : {movie.Value}");
                    Console.Write($"After  : {movieGenre}");
                }

                Console.Write("Would you like to add new move title & genre? (yes or no) : ");
                if(Console.ReadLine() == "yes")
                {
                    var movieTitle = "";
                    var movieGenre = "";

                    Console.Write("Type in the new movie title : ");
                    movieTitle = Console.ReadLine();
                    Console.Write("Type in the new movie genre : ");
                    movieGenre = Console.ReadLine();

                    dicMovies.Add(movieTitle, movieGenre);

                    Console.WriteLine("You've added the movie title and genre");
                    Console.WriteLine($"Before : {movie.Key} / {movie.Value}");
                    Console.WriteLine($"After  : {movieTitle} / {movieGenre}");
                }
            }

            Console.WriteLine("\n\n The list of your movie ---------");
            foreach(var movie in dicMovies)
            {
                Console.WriteLine($"The movie is '{movie.Key}', genre is '{movie.Value}'");
            }

            // c) Count how many movies are either comedies, action or drama. Display the number of the Screen.
            Console.WriteLine("\n\n------------------ C --------------------");
            
            int cntComedies = 0;
            int cntDrama = 0;
            int cntAction = 0;

            foreach(var movie in dicMovies)
            {
                Console.WriteLine(movie.Value);
                switch(movie.Value.ToString().ToLower())
                {
                    case "comedies" : ++cntComedies; break;
                    case "action"   : ++cntAction; break;
                    case "drama"    : ++cntDrama; break;
                    default:break;
                }
            }

            Console.WriteLine($"Comedy movie : {cntComedies}");
            Console.WriteLine($"Action movie : {cntAction}");
            Console.WriteLine($"Drama movie : {cntDrama}");


            // d) Add the movies of question c) into a separate list and display them on the screen.
            Console.WriteLine("\n\n------------------ D --------------------");

            var isFinished = true;

            do
            {
                Console.WriteLine($"Please add another move.");

                Console.Write($"Type in the title of the movie to add : ");
                var movieTitle = Console.ReadLine();

                Console.Write($"Type in the genre of the movie to add : ");
                var movieGenre = Console.ReadLine();

                Console.Write($"Do you want to add more? (yes or no) : ");
                isFinished = (Console.ReadLine() == "yes") ? false : true;

            } while(!isFinished);

            // print
            foreach(var movie in dicMovies)
            {
                Console.WriteLine($"The movie is '{movie.Key}', genre is '{movie.Value}'");
            }
        }
    }
}
